﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class FrmClient : Form
    {
        IPEndPoint ipep;
        Socket client;
        string serverIP;
        int port;
        string fullName;
        // ma trận khóa K có kích thước 2x2 thì m = 2
        // ma trận khóa K có kích thước 3x3 thì m = 3
        int m = 0;
        /// <summary>
        /// Độ dài của chuỗi 
        /// </summary> 
        int lengthMessage = 0;
        // Số bản rõ cần tách, nếu như july, thì numberEncrypt = 2
        int numberEncrypt = 0;


        public FrmClient()
        {
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
        }


        /// <summary>
        /// Tạo ma trận khóa Key
        /// </summary>
        /// <param name="key"></param>
        /// <param name="keyMatrix"></param>
        void getKeyMatrix(String key, int[,] keyMatrix)
        {
            int k = 0;
            for (int i = 0; i < m; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    keyMatrix[i, j] = (key[k]) % 65;
                    k++;
                }
            }
        }

        /// <summary>
        /// Hàm sau mã hóa tin nhắn 
        /// </summary>
        /// <param name="cipherMatrix"></param>
        /// <param name="keyMatrix"></param>
        /// <param name="messageVector"></param>
        void encrypt(int[,] cipherMatrix, int[,] keyMatrix, int[,] messageVector)
        {
            for (int i = 0; i < numberEncrypt; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    cipherMatrix[j, i] = 0;

                    for (int x = 0; x < m; x++)
                    {
                        cipherMatrix[j, i] += keyMatrix[j, x] * messageVector[x, i];
                    }

                    cipherMatrix[j, i] = cipherMatrix[j, i] % 26;
                }
            }
        }

        /// <summary>
        /// Hàm thực thi mật mã Hill 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="key"></param>
        /// <param name="spaces"></param>
        void HillCipher(String message, String key, int[] spaces)
        {
            try
            {
                m = int.Parse(Math.Sqrt(key.Length) + "");
                lengthMessage = message.Length;
                numberEncrypt = lengthMessage / m;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            // Lấy ma trận khóa từ chuỗi khóa 
            int[,] keyMatrix = new int[m, m];
            getKeyMatrix(key, keyMatrix);

            // tạo 1 ma trận chứa m cột và numberEncrypt hàng
            int[,] messageVector = new int[m, numberEncrypt];

            // Tạo vectơ cho message
            // Biến đổi chuỗi text thành ma trận có 3 cột 1 hàng
            for (int i = 0; i < numberEncrypt; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    messageVector[j, i] = (message[j + i * m]) % 65;
                }
            }

            int[,] cipherMatrix = new int[m, numberEncrypt];

            // Hàm sau tạo vectơ được mã hóa 
            encrypt(cipherMatrix, keyMatrix, messageVector);

            String CipherText = "";

            // Tạo văn bản được mã hóa từ vectơ được mã hóa 
            for (int i = 0; i < numberEncrypt; i++)
            {
                for (int j = 0; j < m; j++)
                {
                    CipherText += (char)(cipherMatrix[j, i] + 65);
                }
            }
            String strResult = CipherText;
            for (int j = 0; j < spaces.Length; j++)
            {
                String strTemp = strResult.Insert(spaces[j], " ");
                strResult = strTemp;
            }
            client.Send(Serialize(strResult));
        }


        /// <summary>
        /// Kết nối tới server
        /// </summary>
        void Connect()
        {
            // IP: địa chỉ của server
            ipep = new IPEndPoint(IPAddress.Parse("127.0.0.1"), 1308);
            client = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            try
            {
                client.Connect(ipep);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Không thể kết nối server !" + ex.Message, "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            Thread listen = new Thread(Receive);

            // đặt một giá trị cho biết một chuỗi có phải là một chuỗi nền hay không
            // trả về true nếu chuỗi này đang hoặc sắp trở thành một chuỗi nền
            listen.IsBackground = true;
            listen.Start();
        }

        /// <summary>
        /// Đóng kết nối hiện thời
        /// </summary>
        void CloseConnection()
        {
            client.Close();
        }

        /// <summary>
        /// Gửi tin
        /// </summary>
        void Send()
        {
            String input = txtSend.Text.ToUpper();
            input = input.ToUpper();
            int countSpace = 0;
            for (int k = 0; k < input.Length; k++)
                if (input[k] == 32)
                    countSpace++;
            String message = "";
            int[] spaces = new int[countSpace];
            int space = 0;
            for (int i = 0; i < input.Length; i++)
            {
                if (input[i] != 32)
                    message += input[i];
                else
                {
                    spaces[space] = i;
                    space++;
                }
            }
            String key = txtKey.Text;
            key = key.ToUpper();
            HillCipher(message, key, spaces);
        }

        /// <summary>
        /// Nhận tin
        /// </summary>
        void Receive()
        {
            try
            {
                while (true)
                {
                    byte[] data = new byte[1024 * 5000];
                    client.Receive(data);
                    string message = (string)Deserialize(data);
                    txtReceive.Text = message;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Lỗi: " + ex.Message);
                CloseConnection();
            }
        }

        /// <summary>
        /// Add message vào khung chat
        /// </summary>
        /// <param name="str"></param>
        void AddMessage(string str)
        {
            txtResult.Text += fullName + str + "\r\n";
            txtSend.Clear();
            txtSend.Focus();
        }

        /// <summary>
        /// Phân mảnh
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        byte[] Serialize(object obj)
        {
            MemoryStream stream = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(stream, obj);
            return stream.ToArray();
        }

        /// <summary>
        /// Gom mảnh lại
        /// </summary>
        /// <returns></returns>
        object Deserialize(byte[] data)
        {
            MemoryStream stream = new MemoryStream(data);
            BinaryFormatter formatter = new BinaryFormatter();
            return formatter.Deserialize(stream);
        }
        private void FrmClient_Load(object sender, EventArgs e)
        {

        }
    }
}
